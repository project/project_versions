<?php

declare(strict_types=1);

namespace Drupal\project_versions\Controller;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\project_versions\Services\ProjectVersionsEncryption;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses project_version routes.
 */
class ProjectVersionsController extends ControllerBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The system status encrypt service.
   *
   * @var \Drupal\project_versions\Services\ProjectVersionsEncryption
   */
  protected $encrypt;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('module_handler'),
      $container->get('theme_handler'),
      $container->get('project_versions.encrypt')
    );
  }

  /**
   * ProjectVersionsController constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler.
   * @param \Drupal\project_versions\Services\ProjectVersionsEncryption $encrypt
   *   The System Status encrypt.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, ThemeHandlerInterface $themeHandler, ProjectVersionsEncryption $encrypt) {
    $this->moduleHandler = $moduleHandler;
    $this->themeHandler = $themeHandler;
    $this->encrypt = $encrypt;
  }

  /**
   * Returns the display of the new status.
   *
   * @return array<string, mixed>
   *   The result array.
   */
  protected function data(): array {
    $res = [
      'php_version' => phpversion(),
      'core' => [],
      'contrib' => [],
      'theme' => [],
    ];

    $drupalModules = $this->moduleHandler->getModuleList();
    $drupalThemes = $this->themeHandler->listInfo();

    foreach ($drupalModules as $name => $module) {

      $filename = $module->getPath() . '/' . $module->getFilename();
      $moduleInfo = Yaml::decode(file_get_contents($filename) ?: '');
      if (!is_array($moduleInfo)) {
        $moduleInfo = [];
      }

      // This can happen when you install using composer.
      if (isset($moduleInfo['version']) && $moduleInfo['version'] == 'VERSION') {
        $moduleInfo['version'] = \Drupal::VERSION;
      }

      if (!isset($moduleInfo['version'])) {
        $moduleInfo['version'] = NULL;
      }

      // Do our best to guess the correct drupal version.
      if ($name == 'system' && $moduleInfo['package'] == 'Core') {
        $res['core']['drupal'] = ['version' => $moduleInfo['version']];
      }

      // Skip Core and Field types.
      if ((isset($moduleInfo['package']) && $moduleInfo['package'] == 'Core') || (isset($moduleInfo['package']) && $moduleInfo['package'] == 'Field types') || (isset($moduleInfo['project']) && $moduleInfo['project'] == 'drupal')) {
        continue;
      }

      $delta = $moduleInfo['project'] ?? $name;
      $res['contrib'][$delta] = ['version' => $moduleInfo['version']];

      $props = ['lifecycle', 'lifecycle_link'];
      $res['contrib'][$delta] += array_filter($moduleInfo, fn($key) => in_array($key, $props), ARRAY_FILTER_USE_KEY);
    }

    foreach ($drupalThemes as $name => $theme) {
      $filename = $theme->getPath() . '/' . $theme->getFilename();
      $themeInfo = Yaml::decode(file_get_contents($filename) ?: '');
      if (!is_array($themeInfo)) {
        $themeInfo = [];
      }

      if (!isset($themeInfo['version'])) {
        $themeInfo['version'] = NULL;
      }

      // This can happen when you install using composer.
      if ($themeInfo['version'] == 'VERSION') {
        $themeInfo['version'] = \Drupal::VERSION;
      }

      if (isset($themeInfo['project']) && $themeInfo['project'] == 'drupal') {
        continue;
      }

      $delta = $moduleInfo['project'] ?? $name;
      $res['theme'][$delta] = ['version' => $themeInfo['version']];

      $props = ['lifecycle', 'lifecycle_link'];
      $res['theme'][$delta] += array_filter($themeInfo, fn($key) => in_array($key, $props), ARRAY_FILTER_USE_KEY);
    }

    return $res;
  }

  /**
   * Get the unencrypted result.
   */
  public function load(): JsonResponse {
    return new JsonResponse([
      'data' => $this->data(),
    ]);
  }

  /**
   * Get the encrypted result.
   */
  public function loadEncrypted(): JsonResponse {
    $json = json_encode($this->data(), JsonResponse::DEFAULT_ENCODING_OPTIONS | JSON_THROW_ON_ERROR);

    return new JsonResponse([
      'data' => ProjectVersionsEncryption::encryptOpenssl($json),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $urlToken): AccessResultInterface {
    if ($this->config('project_versions.settings')->get('project_versions_url_token') === $urlToken) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::forbidden();
    }
  }

}
