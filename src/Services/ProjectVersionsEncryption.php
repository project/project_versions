<?php

declare(strict_types=1);

namespace Drupal\project_versions\Services;

use Drupal\Component\Utility\Crypt;

/**
 * Encryption logic for project_versions.
 */
class ProjectVersionsEncryption {

  /**
   * Create a new token.
   */
  public static function getToken(): string {
    return Crypt::randomBytesBase64(16);
  }

  /**
   * Encrypt a plaintext message using openssl.
   */
  public static function encryptOpenssl(string $plainText): string {
    $encryptionKey = \Drupal::config('project_versions.settings')->get('project_versions_encryption_key');
    if (!is_string($encryptionKey)) {
      throw new \RuntimeException('Encryption token is not a string.');
    }

    $key = hash('SHA256', $encryptionKey, TRUE);

    $iv = openssl_random_pseudo_bytes(16);
    $cipherText = openssl_encrypt($plainText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv);

    return base64_encode($iv . $cipherText);
  }

}
