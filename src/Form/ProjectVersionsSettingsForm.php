<?php

declare(strict_types=1);

namespace Drupal\project_versions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure pants settings for this site.
 */
class ProjectVersionsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'project_versions_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('project_versions.settings');

    $form['project_versions_url_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Token'),
      '#default_value' => $config->get('project_versions_url_token'),
      '#size' => 16,
      '#maxlength' => 16,
      '#disabled' => TRUE,
    ];

    $form['project_versions_encryption_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Encryption key'),
      '#default_value' => $config->get('project_versions_encryption_key'),
      '#size' => 16,
      '#maxlength' => 16,
      '#disabled' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   The editable configuration names.
   */
  protected function getEditableConfigNames(): array {
    return ['project_versions.settings'];
  }

}
