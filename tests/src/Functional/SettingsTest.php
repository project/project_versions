<?php

namespace Drupal\Tests\project_versions\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the settings.
 *
 * @group project_versions
 */
class SettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Module.
   *
   * @var string[]
   */
  protected static $modules = ['project_versions'];

  /**
   * Tests the settings form.
   */
  public function testSettings() {
    // Make sure we can't access settings without permissions.
    $this->drupalGet('/admin/config/system/project-versions');
    $this->assertSession()->statusCodeEquals(403);

    $account = $this->createUser(['administer site configuration']);
    $this->drupalLogin($account);

    $this->drupalGet('/admin/config/system/project-versions');
    $this->assertSession()->statusCodeEquals(200);

    // Make sure api keys gets populated.
    $urlToken = $this->getSession()->getPage()->find('css', 'input[name="project_versions_url_token"]');
    $this->assertNotEmpty($urlToken);

    $encryptionKey = $this->getSession()->getPage()->find('css', 'input[name="project_versions_encryption_key"]');
    $this->assertNotEmpty($encryptionKey);
  }

}
