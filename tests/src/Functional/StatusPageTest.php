<?php

namespace Drupal\Tests\project_versions\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the status page.
 *
 * @group project_versions
 */
class StatusPageTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Module.
   *
   * @var string[]
   */
  protected static $modules = ['project_versions'];

  /**
   * Tests the status page.
   */
  public function testStatusPage() {
    // Make sure we can't access settings without permissions.
    $this->drupalGet('/admin/reports/project-versions');
    $this->assertSession()->statusCodeEquals(403);

    $account = $this->createUser(['administer site configuration']);
    $this->drupalLogin($account);

    $this->drupalGet('/admin/reports/project-versions');
    $this->assertSession()->statusCodeEquals(200);
  }

}
