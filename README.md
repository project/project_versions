# Project Versions

Project Versions is a fork of the [System
Status](https://drupal.org/project/system_status) module, but has been
updated for Drupal 10, simplified, and hardened. The data format is no
longer compatible with the Systems Status module.
